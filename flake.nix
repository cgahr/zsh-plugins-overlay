{
  description = "overlay for zsh-plugins";

  inputs = {
    powerlevel10k = {
      type = "github";
      owner = "romkatv";
      repo = "powerlevel10k";
      flake = false;
    };

    nix-shell = {
      type = "github";
      owner = "chisui";
      repo = "zsh-nix-shell";
      flake = false;
    };

    zsh-autosuggestions = {
      type = "github";
      owner = "zsh-users";
      repo = "zsh-autosuggestions";
      flake = false;
    };

    zsh-eza = {
      type = "github";
      owner = "z-shell";
      repo = "zsh-eza";
      flake = false;
    };

    zsh-syntax-highlighting = {
      type = "github";
      owner = "zsh-users";
      repo = "zsh-syntax-highlighting";
      flake = false;
    };
  };

  outputs = {...} @ inputs: let
    mkPlugin = name: {
      inherit name;
      file = "${name}.plugin.zsh";
      src = inputs."${name}";
    };
  in {
    overlay = final: prev: {
      zsh-plugins = {
        powerlevel10k = {
          name = "powerlevel10k";
          file = "powerlevel10k.zsh-theme";
          src = inputs.powerlevel10k;
        };
        nix-shell = mkPlugin "nix-shell";
        zsh-autosuggestions = mkPlugin "zsh-autosuggestions";
        zsh-eza = mkPlugin "zsh-eza";
        zsh-syntax-highlighting = mkPlugin "zsh-syntax-highlighting";
      };
    };
  };
}
